#ifndef GLOBALPARAMETERS_H
#define GLOBALPARAMETERS_H

/************************************file gathering pre-processor settings***********************************/

#define CommandError 1
#define FunctionFailed -1
#define FunctionSuccess 0
#define FunctionWarning 1

/*--------------------Warning settings-------------------------------*/
#define WarningNotSupported         "Non supported functionnality"
#define JsonDataNotFound            "Json data could not be retrieved"
/*--------------------Display settings-------------------------------*/
#define BackgroundLightGreen        "Background-color: rgb(144,238,144)"
#define BackgroundLightRed          "Background-color: rgb(255,100,100)"
#define BackgroundInfoColor         "Background-color: yellow"
#define BackgroundCyan              "Background-color: cyan"
#define BackgroundLightBlue         "Background-color: rgb(173,216,230)"
#define BackgroundWhite             "Background-color: white"
#define BackgroundOrange            "Background-color: orange"
/*---------------------Miscellaneous---------------------------------*/
//1st january 1970 00:00:00
#define DummyDate                   QDateTime::fromString("M1d1y7000:00:00", "'M'M'd'd'y'yyhh:mm:ss")
/*--------------------Default settings-------------------------------*/
#define DefaultTimerSec             5
#define DefaultPortNumber           26657
#define DefaultHistoricPath         "/home/ausy/go/src/github.com/hussein1571/smart/n1.txt"
#define DefaultCaseSensitive        true
#define DefaultBlinking             false
#define DefaultTransactionsFlush    false
#define DefaultBlockchainStop       false
#define DefaultGatewayMode          false
/*--------------------Display settings-------------------------------*/
#define InfoFieldsNb                9
/*-------------------------------------------------------------------*/
#define GatewayPeriodMs             5000


#endif // GLOBALPARAMETERS_H
