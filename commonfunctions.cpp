#include "commonfunctions.h"



void NotSupportedWarning(const QString Warning)
{
    QDialog *popup = new QDialog();
    QMessageBox::information(popup, "Block Chain Viewer", Warning);
}


QString EnterPassword()
{
    QString PW = "";
    QLineEdit *PW_Line = new QLineEdit();

    QMessageBox::information(PW_Line, "Block Chain Viewer", "");
    PW_Line->setEchoMode(QLineEdit::Password);
    PW = PW_Line->text();
    return PW;
}


void CloseAllTerminals()
{
    QProcess process;
    process.start("./KillAll.sh");
    process.waitForFinished();
    process.close();
}


int GoToDir(const QString& path)
{
    QProcess process;
    process.start("cd " + path);
    process.waitForFinished();
    QByteArray outputStandardError = process.readAllStandardError();
    process.close();
    //No output for cd request, just error output needs to be checked
    if (outputStandardError == "")
    {return FunctionSuccess;}
    else
    {return FunctionFailed;}
}


QDateTime TimestampToDate(const QString& timestamp)
{
    QDateTime Date;
    //Adapt the format given by TTN: on 13 digits
    QString s = FormatTimeStamp(timestamp);
    bool isok = false;
    long int i_timestamp = s.toLong(&isok);
    //Check that the timestamp could be converted to an integer
    if(isok == true && i_timestamp > 0)
    {
        Date.setTime_t(i_timestamp);
    }
    else
    {
        NotSupportedWarning("Timestamp could not be converted to an integer");
    }
    return Date;
}


QString FormatTimeStamp(const QString& timestamp)
{
    QString s = timestamp;
    //remove "." and what is after, to chop the ms
    //s.truncate(s.lastIndexOf(QChar('.')));
    //if no "." in the format, check that length is not > 10
    int l = s.length();
    if (l > 10)
    {
        s.chop(l-10);
    }
    return s;
}


static QDateTime GetEarliestDate(const QVector<QDateTime>& DatesVector)  //not to be used if dates can be put in chronological order
{
    QDateTime EarlierDate = DatesVector[0];
    int l = DatesVector.length();
    for (int i = 0; i < l; i++)
    {
        if (DatesVector[i] < EarlierDate)
        {EarlierDate = DatesVector[i];}
    }
    return EarlierDate;
}


static QDateTime GetLatestDate(const QVector<QDateTime>& DatesVector)   //not to be used if dates can be put in chronological order
{
    QDateTime EarlierDate = DatesVector[0];
    int l = DatesVector.length();
    for (int i = 0; i < l; i++)
    {
        if (DatesVector[i] > EarlierDate)
        {EarlierDate = DatesVector[i];}
    }
    return EarlierDate;
}


void ChronoDates(QVector<QDateTime> DatesVector)
{
    int l = DatesVector.length();
    QDateTime IntermediateDate;
    for (int i = 0; i < l-1; i++)
    {
        if (DatesVector[i] > DatesVector[i+1])
        {
            IntermediateDate = DatesVector[i+1];
            DatesVector[i+1] = DatesVector[i];
            DatesVector[i] = IntermediateDate;
        }
    }
    for (int j = l-1; j > 1; j--)
    {
        if (DatesVector[j] < DatesVector[j-1])
        {
            IntermediateDate = DatesVector[j];
            DatesVector[j] = DatesVector[j-1];
            DatesVector[j-1] = IntermediateDate;
        }
    }
}

//Function overloading
void ChronoDates(QVector<QString> DatesString)
{
    int l = DatesString.length();
    QString IntermediateDate;
    for (int i = 0; i < l-1; i++)
    {
        if (DatesString[i].toInt() > DatesString[i+1].toInt())
        {
            IntermediateDate = DatesString[i+1];
            DatesString[i+1] = DatesString[i];
            DatesString[i] = IntermediateDate;
        }
    }
    for (int j = l-1; j > 1; j--)
    {
        if (DatesString[j].toInt() < DatesString[j-1].toInt())
        {
            IntermediateDate = DatesString[j];
            DatesString[j] = DatesString[j-1];
            DatesString[j-1] = IntermediateDate;
        }
    }
}
//

void SetDelay(int delay_sec)
{
    QTime dieTime= QTime::currentTime().addSecs(delay_sec);
    while (QTime::currentTime() < dieTime)
    {}
}


static int GetProcessNb(int PortNumber)
{
    int ProcessNb;
    QProcess process;
    QString command = "netstat -pnlt | grep ':" + QString::number(PortNumber) + "'";
    process.start(command);
    process.waitForFinished();
    QByteArray outputStandard = process.readAll();
    //Be careful: that command returns a non relevant error message that will be ignored here
    process.close();
    QString response = outputStandard;
    int end = response.indexOf("/sd");
    response.truncate(end);
    int begin = response.lastIndexOf(" ");
    //Port number begin: begin+1
    //Port number end: end-1
    //Port number length = Port number end - Port number begin + 1 = (end-1)-(begin+1)+1 = end-begin-1
    ProcessNb = response.mid(begin+1, end-begin-1).toInt();
    process.close();
    return ProcessNb;
}

void KillSdProcess(int PortNumber)
{
    QProcess process;
    int ProcessNb = GetProcessNb(PortNumber);
    if (ProcessNb > 0)
    {
        QString command = "kill -9 " + QString::number(ProcessNb);
        process.start(command);
        process.waitForFinished();
        QByteArray outputStandardError = process.readAllStandardError();
        process.close();
        if (outputStandardError != "")
        {
            NotSupportedWarning("sd process could not be stopped");
        }
        process.close();
    }
    else
    {
        NotSupportedWarning("sd process not found");
    }

}


