#include "blockchainviewer.h"
#include "ui_blockchainviewer.h"
#include "timestampsthread.h"



using namespace std;

void RemoveNode(Ui::BlockChainViewer* ui);
int InitGenesisFile(Ui::BlockChainViewer*);
int AccountConfiguration(Ui::BlockChainViewer*);
void Enable_Password(Ui::BlockChainViewer* ui, bool);
void DebugVisible(Ui::BlockChainViewer*, bool);


BlockChainViewer::BlockChainViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::BlockChainViewer), timer (new QTimer), NetworkManager (new QNetworkAccessManager), NetworkReply(0), GatewayTimer(new QTimer)
{
    ui->setupUi(this);
    connect(ui->NodeTypeSelector, SIGNAL(highlighted(int)), this, SLOT(DisplayInfo()));
    //connect(ui->Node_Name_Label, SIGNAL((QString)), this, SLOT(DisplayInfo()));
    //connect(ui->ChainID, SIGNAL(selectionChanged(int)), this, SLOT(DisplayInfo()));
    ui->TransactionDate->showToday();
    /*------------------Initialize tab part-----------------------------*/
    ui->NodeName->setText("node1");
    ui->ChainID->setText("smartcity");
    ui->AccountName->setText("n1");
    ui->AccountBalance->setText("10000");
    ui->PWEdit->setText("12345678");
    ui->PWEdit_2->setText("12345678");
    /*-------------------------------------------------------------*/
    DebugVisible(ui, true);
    /*------------------Display part---------------------------------*/
    ui->TransactionDate->showToday();
    ui->TransactionTime->setTime(QTime::currentTime());
    ui->FirstTransactionDate->setDisplayFormat("dd/MM/yyyy HH:mm:ss");
    ui->LatestTransactionDate->setDisplayFormat("dd/MM/yyyy HH:mm:ss");
    ui->TransactionDateTime->setDisplayFormat("dd/MM/yyyy HH:mm:ss");
    ui->tabWidget->setTabText(0, "Initialize");
    ui->tabWidget->setTabText(1, "Get Information");
    ui->tabWidget->setTabText(2, "Single Block Info");
    ui->tabWidget->setTabText(3, "Configuration");
    /*----------non working Init phase is hidden, to be done by launching "launch.sh" ---------------*/
    ui->tabWidget->removeTab(0);
    /*-----------------------------------------------------------------------------------------------*/
    //ui->tabWidget->removeTab(2);
    /*-----------------------------------------------*/

    ui->QuitButtonTab1->setIcon(QIcon::fromTheme("window-close"));
    ui->QuitButtonTab2->setIcon(QIcon::fromTheme("window-close"));
    ui->QuitButtonTab3->setIcon(QIcon::fromTheme("window-close"));
    ui->QuitButtonTab4->setIcon(QIcon::fromTheme("window-close"));
    //per default, data display is launched
    ui->StopStartLabel->setText("Stop block data display");
    ui->StartStopButton->setIcon(QIcon::fromTheme("media-playback-stop"));
    //per default, gateway mode is not activated
    ui->StopStartGatewayLabel->setText("Start gateway mode");
    ui->StartStopGatewayButton->setIcon(QIcon::fromTheme("media-playback-start"));

    ui->Info1->setStyleSheet(BackgroundInfoColor);
    ui->Info2->setStyleSheet(BackgroundInfoColor);
    ui->Info3->setStyleSheet(BackgroundInfoColor);
    ui->Info4->setStyleSheet(BackgroundInfoColor);
    ui->Info5->setStyleSheet(BackgroundInfoColor);
    ui->Info6->setStyleSheet(BackgroundInfoColor);
    ui->Info7->setStyleSheet(BackgroundInfoColor);
    ui->Info8->setStyleSheet(BackgroundInfoColor);
    ui->Info9->setStyleSheet(BackgroundInfoColor);
    ui->TransactionDateTime->setStyleSheet(BackgroundCyan);
    ui->TimestampFound->setStyleSheet(BackgroundCyan);
    /*-------------------------------------------------------------*/
    //set useful tab as default tab
    ui->tabWidget->setCurrentWidget(ui->TabInfo);

    //Enable_Password(ui, false);
    ui->PWEdit->setEchoMode(QLineEdit::Password);
    ui->PWEdit_2->setEchoMode(QLineEdit::Password);
    /*-----------------------set default display---------------------------------------*/
    SetDefaultConfigDisplay();
    /*------------------Timer part---------------------------------*/
    //No need to wait for timer to be elapsed for the first call
    timer->start(100);
    connect(timer, SIGNAL(timeout()), this, SLOT(GetBlockChainInfo()));
    connect(NetworkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(getFinished(QNetworkReply*)));
    connect(GatewayTimer, SIGNAL(timeout()), this, SLOT(LaunchGatewayMode()));
}


void BlockChainViewer::GetBlockChainInfo()          //function called periodically each TimerPeriod_ms
{
    ClearPeriodicInfo();
    TimestampsRequest();
    GetURLInfo();

    timer->start(SavedInfo.TimerPeriod_ms);
}

void BlockChainViewer::LaunchGatewayMode()
{
    /*--------------------------- Using gateway mode---------------------------------*/
    if (ui->CurrentBlockNumber->text().toInt() > 2)
    {
        BroadcastTransaction();                         //reading the n1.txt, sending the transactions
    }
    else
    {
        NotSupportedWarning("Please start blockchain or deactivate gateway mode");
    }
    GatewayTimer->start(GatewayPeriodMs);   //try to check a transaction each 10s
}

void BlockChainViewer::BroadcastTransaction()
{
    static int currentLine = 1;
    QString TransactionPath = SavedInfo.HistoricPath;
    QFile *TransactionsFile = new QFile(TransactionPath);
    if(TransactionsFile->open(QIODevice::ReadOnly | QIODevice::Text) == true)
    {
        QByteArray FileData;
        FileData = TransactionsFile->readAll(); // read all the data into the byte array
        QTextStream flux(FileData);
        QString Line;
        int i = 1;
        while(i<=currentLine)
        {
            if (flux.atEnd())
            {
                Line = "";
                break;          //we shall not continue if no more transaction yet available
            }
            Line = flux.readLine();
            i++;
        }
        TransactionsFile->close(); // close the file handle.
        QStringList List = Line.split(" ",QString::SkipEmptyParts);
        QString path = QDir::currentPath();
        QString ShellPath = path + "/Broadcast.sh";
        QFile *ShellFile = new QFile(ShellPath);
        QString OriginalText;
        bool b = ShellFile->open(QIODevice::ReadWrite | QIODevice::Text);
        bool ModifyShell = b && (List.count() == 7);
        if (ModifyShell == true)
        {
            QByteArray FileData;
            FileData = ShellFile->readAll();
            QString text(FileData);
            OriginalText = text;
            ShellFile->resize(0);
            text.replace(QString("timestamp"), List[4]); // replace text in string
            text.replace(QString("diocarbon"), List[0]); // replace text in string
            text.replace(QString("monocarbon"), List[1]); // replace text in string
            text.replace(QString("ph"), List[2]); // replace text in string
            text.replace(QString("turbi"), List[3]); // replace text in string
            text.replace(QString("country"), List[5]); // replace text in string
            text.replace(QString("region"), List[6]); // replace text in string
            ShellFile->seek(0); // go to the beginning of the file
            ShellFile->write(text.toUtf8()); // write the new text back to the file
            ShellFile->close(); // close the file handle.

            QProcess process;
            process.setProcessChannelMode(QProcess::MergedChannels);
            process.start("sh", QStringList() << "Broadcast.sh");
            process.waitForFinished();
            QByteArray StandardOutput = process.readAll();
            QByteArray ErrorOutput = process.readAllStandardError();
            ui->BlockInfoText->setText(StandardOutput);
            ui->BlockInfoText->append(ErrorOutput);
            process.close();
            if (!StandardOutput.startsWith("ERROR"))
            {
                //Going to next line only if transaction could be done
                ui->ComputingTime->setText(QString::number((currentLine)));
                currentLine++;
            }
            //No need to write original text if no valid transaction was found
            //replacing the original text (with timestamp, region...) for the next queuing transaction
            ShellFile->open(QIODevice::ReadWrite | QIODevice::Text);
            ShellFile->resize(0);
            ShellFile->seek(0); // go to the beginning of the file
            ShellFile->write(OriginalText.toUtf8()); // write the new text back to the file
            ShellFile->close(); // close the file handle.
        }
        delete ShellFile;
        ShellFile = 0;
    }
}


void BlockChainViewer::ClearPeriodicInfo()
{
    ui->FirstTransactionDate->setStyleSheet(BackgroundWhite);
    ui->LatestTransactionDate->setStyleSheet(BackgroundWhite);
    ui->TransactionsCounter->setStyleSheet(BackgroundWhite);
    //Previous block
    ui->nbTx_before->setStyleSheet(BackgroundWhite);
    //Current block
    ui->nbTx_current->setStyleSheet(BackgroundWhite);
    if (SavedInfo.BlockInfoBlinking == true)
    {
        //Previous block
        ui->PreviousBlockNumber->clear();
        ui->LastHash_before->clear();
        ui->Hash_before->clear();
        ui->nbTx_before->clear();
        ui->BlockTime_before->clear();
        //Current block
        ui->CurrentBlockNumber->clear();
        ui->LastHash_current->clear();
        ui->Hash_current->clear();
        ui->nbTx_current->clear();
        ui->BlockTime_current->clear();
    }
}


void BlockChainViewer::getFinished(QNetworkReply* reply)
{
    QByteArray res = reply->readAll();
    QJsonDocument JsonOutput = QJsonDocument::fromJson(res);
    QJsonObject JsonInfo = JsonOutput.object();
    try
    {
        QJsonObject IntermediateObject = JsonInfo["result"].toObject();
        QJsonObject FinalObject = IntermediateObject["response"].toObject();
        QString BlockHeight = FinalObject.value("last_block_height").toString();
        //If not filtered, a void string is displayed: reason to be investigated
        if(BlockHeight != "")
        {
            //Block 0 does not exist
            ui->PreviousBlockNumber->setText((BlockHeight.toInt()-1) > 0 ? QString::number(BlockHeight.toInt()-1) : "---");
            ui->CurrentBlockNumber->setText(BlockHeight);
        }
    }
    catch (exception e)
    {
        ui->PreviousBlockNumber->setText("not found");
        ui->CurrentBlockNumber->setText("not found");
    }
    /*To be analyzed if shall be kept or not, goal is avoiding memory leak*/
    //reply->deleteLater();
    //delete reply;
    //
    reply = 0;
    GetBlockChainInfo_Part2();
}


void BlockChainViewer::GetBlockChainInfo_Part2()
{
    int LastBlock = ui->CurrentBlockNumber->text().toInt();

    //No need to ask info for block 0 (which does not exist)
    if (LastBlock > 1)
    {
        QByteArray BeforeLastBlockInfo = GetGeneralBlockInfo(LastBlock-1);
        FillBlockGeneralInfo(BeforeLastBlockInfo, BeforeLast);
    }
    QByteArray LastBlockInfo = GetGeneralBlockInfo(LastBlock);
    FillBlockGeneralInfo(LastBlockInfo, Last);
    HashCompare();
}


QByteArray BlockChainViewer::GetGeneralBlockInfo(int BlockNumber)
{
    QProcess process;
    QString command = "scli query block " + QString::number(BlockNumber);
    process.start(command);
    process.waitForFinished();
    QByteArray output = process.readAll();
    output+=process.readAllStandardError();
    process.close();
    return output;
}


void BlockChainViewer::GetURLInfo()
{
    QString URLstring = "http://localhost:26657/abci_info";
    QUrl BlockHeightURL = QUrl(URLstring);
    QNetworkRequest request = QNetworkRequest(BlockHeightURL);
    //QNetworkReply *reply = new QNetworkReply;     -----------> does not compile, abstract object cant be initiated
    NetworkReply = NetworkManager->get(request);
}


void DebugVisible(Ui::BlockChainViewer* ui, bool IsVisible)
{
    ui->comboTimes->setVisible(IsVisible);
    ui->BlockInfoText->setVisible(IsVisible);
    ui->ComputingTime->setVisible(IsVisible);
}


void BlockChainViewer::QuitProgram()
{
    QString path = QDir::currentPath();
    GoToDir(path);
    if (SavedInfo.BlockchainStopQuit == true)
    {
        KillSdProcess(SavedInfo.PortNumber);
    }
    if (SavedInfo.EmptyTransactionsQuit == true)
    {
        DeleteTransactionsHistoric();
    }
    qApp->quit();
}

void Enable_Password(Ui::BlockChainViewer* ui, bool b)
{
    ui->PWEdit->setVisible(b);
    ui->PWEdit_2->setVisible(b);
    ui->PasswordLabel->setVisible(b);
    ui->PasswordLabel_2->setVisible(b);
}


BlockChainViewer::~BlockChainViewer()
{
    delete ui;
    delete timer;
    delete GatewayTimer;
    delete NetworkManager;
}


void BlockChainViewer::DisplayInfo()
{
    ui->InfoDisplay->append("Info");
}


void BlockChainViewer::CreationFunction()
{
    //this->EnableTab2(false);
    if (ui->NodeTypeSelector->currentIndex() == 1)
    {
        NotSupportedWarning(WarningNotSupported);
    }
    else if (ui->NodeName->toPlainText() == "" || ui->ChainID->toPlainText() == "" || ui->AccountName->toPlainText() == "" || ui->PWEdit->text() == "" || ui->PWEdit_2->text() == "")
    {
        NotSupportedWarning("Please fill all fields out");
    }
    else if (ui->PWEdit->text() != ui->PWEdit_2->text())
    {
        NotSupportedWarning("Passwords differ");
    }
    else
    {
        QString path = QDir::currentPath();
        QString ShellPath = path + "/ConfigBlockChain.sh";
        QFile *ShellFile = new QFile(ShellPath);
        QString OriginalText;
        bool b = ShellFile->open(QIODevice::ReadWrite | QIODevice::Text);
        if(b == true)
        {
           // QTextStream stream(JsonFile);
            QByteArray FileData;
            FileData = ShellFile->readAll(); // read all the data into the byte array
            QString text(FileData); // add to text string for easy string replace
            OriginalText = text;

            text.replace(QString("nodename"), QString(ui->NodeName->toPlainText())); // replace text in string
            text.replace(QString("chainname"), QString(ui->ChainID->toPlainText())); // replace text in string
            text.replace(QString("username"), QString(ui->AccountName->toPlainText())); // replace text in string
            text.replace(QString("PASSWORD"), QString(ui->PWEdit->text())); // replace text in string
            ShellFile->seek(0); // go to the beginning of the file
            ShellFile->write(text.toUtf8()); // write the new text back to the file
            ShellFile->close(); // close the file handle.
        }
        else
        {
            NotSupportedWarning("Not able to launch shell script");
        }
        QProcess process;
        GoToDir(path);



        //Run the shell script
        //QStringList env = QProcess::systemEnvironment();
//        env << path + "\\temp";
//        process.setEnvironment(env);
        process.start("./ConfigBlockChain.sh");
//        //process.start("./launch_Qt_essai.sh");
//        //process.start("sd start");
        process.waitForFinished();
        QByteArray output1 = process.readAll();
        QByteArray output2 = process.readAllStandardError();
        QByteArray output3 = process.readAllStandardOutput();
        ui->InfoDisplay->append(output1);
        ui->InfoDisplay->append(output2);
        ui->InfoDisplay->append(output3);
        process.close();

        if (b == true)
        {
            ShellFile->open(QIODevice::ReadWrite | QIODevice::Text);
            ShellFile->write(OriginalText.toUtf8()); // write the new text back to the file
            ShellFile->close(); // close the file handle.
        }


        //If we want to periodically send the timestamps request, not done for the moment
//        TimeStampsThread* CreationThread = new TimeStampsThread();
//        CreationThread->start();
        //

//        this->EnableTab2(true, ui);
        Enable_Password(ui, false);
    }
}


int InitGenesisFile(Ui::BlockChainViewer* ui)
{
    //sd init < moniker> --chain-id smartcity
    QProcess process;
    QString command = "sd init " + ui->NodeName->toPlainText() + " --chain-id " + ui->ChainID->toPlainText();
    process.start(command);
    process.waitForFinished();
    QByteArray output1 = process.readAll();
    QByteArray output2 = process.readAllStandardError();
    QByteArray output3 = process.readAllStandardOutput();
    ui->ConsoleOutput->append(output1);
    process.close();
    if (output1 == "")
    {
        ui->ConsoleOutput->append(output2);
        return CommandError;
    }
    return 0;
}


int AccountCreation(Ui::BlockChainViewer* ui)
{
    //scli keys add jack
    QProcess process;
    QString command = "scli keys add " + ui->AccountName->toPlainText();
    process.start(command);
    process.waitForFinished();
    QByteArray output1 = process.readAll();
    QByteArray output2 = process.readAllStandardError();
    QByteArray output3 = process.readAllStandardOutput();
    ui->ConsoleOutput->append(output1);
    QProcess::ProcessError e = process.error();
    process.close();
    if (output1 == "")
    {
        ui->ConsoleOutput->append(output2);
        return CommandError;
    }
    return 0;
}




void BlockChainViewer::on_ResetButton_clicked()
{
    ui->NodeTypeSelector->setCurrentIndex(0);
    ui->ChainID->setText("");
}


void RemoveNode(Ui::BlockChainViewer* ui)
{
    QProcess process;
    process.start("rm -rf ~/.s*");
    process.waitForFinished();
    QByteArray output1 = process.readAll();
    QByteArray output2 = process.readAllStandardError();
    QByteArray output3 = process.readAllStandardOutput();
    ui->ConsoleOutput->append(output1);
    process.close();
}


int BlockChainViewer::TimestampsRequest()
{
    QProcess process;
    static QByteArray LatestOutput = "first call";
    process.start("scli query " + ui->ChainID->toPlainText() + " times");
    process.waitForFinished();
    QByteArray CurrentOutput = process.readAll();
    process.close();
    if(CurrentOutput != "" && (CurrentOutput != LatestOutput || LatestOutput == "first call"))
    {
        QJsonDocument JsonOutput = QJsonDocument::fromJson(CurrentOutput);
        QJsonArray TimestampsArray = JsonOutput.array();
        QJsonValue jsonTime;
        QString timestamp;
        TransactionDates.clear();
        OriginalTimestamps.clear();
        int TransactionsNumber = TimestampsArray.count();
        ui->TransactionsCounter->setText(QString::number(TransactionsNumber));
        for (int i =0; i < TransactionsNumber; i++)
        {
            jsonTime = TimestampsArray[i];
            timestamp = TimestampsArray[i].toString();
            OriginalTimestamps.push_back(timestamp);
            TransactionDates.push_back(TimestampToDate(timestamp));
        }
        ChronoDates(TransactionDates);
        ChronoDates(OriginalTimestamps);
        //If vector is empty, first and last methods will have the application crashed
        if (TransactionsNumber > 0)
        {
            ui->FirstTransactionDate->setDateTime(TransactionDates.first());
            ui->LatestTransactionDate->setDateTime(TransactionDates.last());
        }
        else
        {
            NoTransactionFoundDisplay("No transaction found", "orange");
            return FunctionFailed;
        }
    }
    else if(CurrentOutput == "")
    {
        NoTransactionFoundDisplay("Query failed", "red");
        return FunctionFailed;
    }
    LatestOutput = CurrentOutput;
    return FunctionSuccess;
}


void BlockChainViewer::NoTransactionFoundDisplay(const QString& CounterText, const QString& color)
{
    ui->TransactionsCounter->setText(CounterText);
    ui->FirstTransactionDate->setDateTime(DummyDate);
    ui->LatestTransactionDate->setDateTime(DummyDate);
    if(color == "red")
    {
        ui->FirstTransactionDate->setStyleSheet(BackgroundLightRed);
        ui->LatestTransactionDate->setStyleSheet(BackgroundLightRed);
        ui->TransactionsCounter->setStyleSheet(BackgroundLightRed);
    }
    if(color == "orange")
    {
        ui->FirstTransactionDate->setStyleSheet(BackgroundOrange);
        ui->LatestTransactionDate->setStyleSheet(BackgroundOrange);
        ui->TransactionsCounter->setStyleSheet(BackgroundOrange);
    }
}



void BlockChainViewer::on_GetInfoButton_clicked()
{
    //initiate block chain info to dummy values
    ui->TransactionDateTime->setDateTime(DummyDate);
    ui->TimestampFound->clear();
    timer->stop();
    int TransactionsFound = TimestampsRequest();
    ResetTransactionDisplay(TransactionsFound);

    if (TransactionsFound == FunctionSuccess)
    {
        QString ChosenTimestamp;
        QDateTime FoundDate;
        ui->GetInfoOutput->clear();
        if (ui->SelectedRadioButton->isChecked())
        {
            QDateTime UserDateTime;
            //UserDateTime.
            UserDateTime.setDate(ui->TransactionDate->selectedDate());
            UserDateTime.setTime(ui->TransactionTime->time());
            int TimeStampIndex = GetClosestTimestamp(UserDateTime);
            ui->TimestampFound->setText(OriginalTimestamps[TimeStampIndex]);
            FoundDate = TransactionDates[TimeStampIndex];
            ChosenTimestamp = OriginalTimestamps[TimeStampIndex];
        }
        else if (ui->FirstRadioButton->isChecked())
        {
            FoundDate = TransactionDates.first();
            ChosenTimestamp = OriginalTimestamps.first();
            ui->TimestampFound->setText(OriginalTimestamps.first());
        }
        else if (ui->LastRadioButton->isChecked())
        {
            FoundDate = TransactionDates.last();
            ChosenTimestamp = OriginalTimestamps.last();
            ui->TimestampFound->setText(OriginalTimestamps.last());
        }
        ui->TransactionDateTime->setDateTime(FoundDate);
        QProcess process;
        process.start("scli query smartcity GiveInfo " + ChosenTimestamp);
        process.waitForFinished();
        QByteArray Info = process.readAll();
        ui->GetInfoOutput->append(Info);
        process.close();

        QJsonDocument JsonOutput = QJsonDocument::fromJson(Info);
        QJsonObject JsonInfo = JsonOutput.object();
        int result = FillInfo(JsonInfo);
        if (result == FunctionFailed)
        {
            NotSupportedWarning(JsonDataNotFound);
        }
    }
    else
    {
        NotSupportedWarning("No timestamp yet found");
    }
    timer->start(SavedInfo.TimerPeriod_ms);
}


int BlockChainViewer::FillInfo(QJsonObject& JsonInfo)
{
    QStringList InfoLabels = JsonInfo.keys();
    for (int i = 0; i<SavedInfo.unwantedInfo.count(); i++)
    {
        if (SavedInfo.CaseSensitive == true)
        {
            JsonInfo.remove(SavedInfo.unwantedInfo[i]);
        }
        else
        {
            for (int j = 0; j < InfoLabels.count(); j++)
            {
                if (QString::compare(SavedInfo.unwantedInfo[i], InfoLabels[j], Qt::CaseInsensitive) == 0)
                {
                    JsonInfo.remove(InfoLabels[j]);
                }
            }
        }
    }
    InfoLabels = JsonInfo.keys();
    QJsonValue InfoValue;
    int DataNumber = InfoLabels.count();
    if (DataNumber == 0)
    {return FunctionFailed;}
    else if (DataNumber > InfoFieldsNb)
    {
        NotSupportedWarning("Not enough fields for displaying all transaction info");
        //FunctionWarning unused, but may be used later, created here for avoiding double warning message, and not return FunctionSuccess
        return FunctionWarning;
    }
    else
    {
        try
        {
            switch(DataNumber)
            {
                case 9:
                    InfoValue = JsonInfo.value(InfoLabels[8]);
                    ModifyLabel(ui->InfoLabel9, InfoLabels[8]);         //visibilty is automatically set to true
                    ModifyTextEdit(ui->Info9, InfoValue.toString());    //visibilty is automatically set to true
    //                ui->InfoLabel9->setText(InfoLabels[8]);
    //                ui->Info9->setText(InfoValue.toString());
    //                ui->Info9->setVisible(true);
    //                ui->InfoLabel9->setVisible(true);
                case 8:
                    InfoValue = JsonInfo.value(InfoLabels[7]);
                    ModifyLabel(ui->InfoLabel8, InfoLabels[7]);
                    ModifyTextEdit(ui->Info8, InfoValue.toString());
                case 7:
                    InfoValue = JsonInfo.value(InfoLabels[6]);
                    ModifyLabel(ui->InfoLabel7, InfoLabels[6]);
                    ModifyTextEdit(ui->Info7, InfoValue.toString());
                case 6:
                    InfoValue = JsonInfo.value(InfoLabels[5]);
                    ModifyLabel(ui->InfoLabel6, InfoLabels[5]);
                    ModifyTextEdit(ui->Info6, InfoValue.toString());
                case 5:
                    InfoValue = JsonInfo.value(InfoLabels[4]);
                    ModifyLabel(ui->InfoLabel5, InfoLabels[4]);
                    ModifyTextEdit(ui->Info5, InfoValue.toString());
                case 4:
                    InfoValue = JsonInfo.value(InfoLabels[3]);
                    ModifyLabel(ui->InfoLabel4, InfoLabels[3]);
                    ModifyTextEdit(ui->Info4, InfoValue.toString());
                case 3:
                    InfoValue = JsonInfo.value(InfoLabels[2]);
                    ModifyLabel(ui->InfoLabel3, InfoLabels[2]);
                    ModifyTextEdit(ui->Info3, InfoValue.toString());
                case 2:
                    InfoValue = JsonInfo.value(InfoLabels[1]);
                    ModifyLabel(ui->InfoLabel2, InfoLabels[1]);
                    ModifyTextEdit(ui->Info2, InfoValue.toString());
                case 1:
                    InfoValue = JsonInfo.value(InfoLabels[0]);
                    ModifyLabel(ui->InfoLabel1, InfoLabels[0]);
                    ModifyTextEdit(ui->Info1, InfoValue.toString());
            }
            return FunctionSuccess;
        }
        catch (exception e)
        {
            return FunctionFailed;
        }
    }
}


int  BlockChainViewer::GetClosestTimestamp(const QDateTime& DateTime)
{
    int index;
    QDateTime LowerDate = TransactionDates.first();
    QDateTime UpperDate = TransactionDates.last();
    if (DateTime < LowerDate)
    {
        return 0;
    }
    else if (DateTime > UpperDate)
    {
        return (TransactionDates.count() -1);
    }
    for (int i = 0; i<TransactionDates.count(); i++)
    {
        if (TransactionDates[i] >= DateTime)
        {
            LowerDate = TransactionDates[i-1];
            UpperDate = TransactionDates[i];
            index = i;
            break;
        }
    }
    int LowerDiff = LowerDate.msecsTo(DateTime);
    int UpperDiff = DateTime.msecsTo(UpperDate);
    if (LowerDiff < UpperDiff)
    {
        index--;
    }
    return index;
}

void BlockChainViewer::ModifyTextEdit(QTextEdit* textedit, const QString text, const bool visible)
{
    textedit->setVisible(visible);
    textedit->setText(text);
}

void BlockChainViewer::ModifyLabel(QLabel* label, const QString text, const bool visible)
{
    label->setVisible(visible);
    label->setText(text);
}

void  BlockChainViewer::ResetTransactionDisplay(const int TransactionsFound)
{
    //If no transaction has been found, revert to original display
    //otherwise, hide all fields to have a dynamic display
    bool display = TransactionsFound == FunctionFailed ? true : false;
    ModifyLabel(ui->InfoLabel1, "Field 1", display);
    ModifyTextEdit(ui->Info1, "", display);
    ModifyLabel(ui->InfoLabel2, "Field 2", display);
    ModifyTextEdit(ui->Info2, "", display);
    ModifyLabel(ui->InfoLabel3, "Field 3", display);
    ModifyTextEdit(ui->Info3, "", display);
    ModifyLabel(ui->InfoLabel4, "Field 4", display);
    ModifyTextEdit(ui->Info4, "", display);
    ModifyLabel(ui->InfoLabel5, "Field 5", display);
    ModifyTextEdit(ui->Info5, "", display);
    ModifyLabel(ui->InfoLabel6, "Field 6", display);
    ModifyTextEdit(ui->Info6,  "", display);
    ModifyLabel(ui->InfoLabel7, "Field 7", display);
    ModifyTextEdit(ui->Info7, "", display);
    ModifyLabel(ui->InfoLabel8, "Field 8", display);
    ModifyTextEdit(ui->Info8, "", display);
    ModifyLabel(ui->InfoLabel9, "Field 9", display);
    ModifyTextEdit(ui->Info9, "", display);
}



void BlockChainViewer::on_GetBlockInfoButton_clicked()
{
   GetSingleBlockInfo();
}

void BlockChainViewer::on_BlockNumber_editingFinished()
{
    GetSingleBlockInfo();
}

void BlockChainViewer::GetSingleBlockInfo()
{
    ClearSelectedBlockDisplay();
    timer->stop();
    QByteArray BlockGeneralInfo = GetGeneralBlockInfo(ui->BlockNumber->value());
    FillBlockGeneralInfo(BlockGeneralInfo, Selected);
    ui->ChosenBlockInfoText->setText(BlockGeneralInfo);
    timer->start();
}


void BlockChainViewer::ClearSelectedBlockDisplay()
{
    ui->ChosenBlockInfoText->clear();
    ui->LastHash_selected->clear();
    ui->Hash_selected->clear();
    ui->nbTx_selected->clear();
    ui->TimeBlock_selected->clear();
    ui->nbTx_selected->setStyleSheet(BackgroundWhite);
}


void BlockChainViewer::FillBlockGeneralInfo(const QByteArray JsonData, const BlockType bloctype)
{
    QJsonDocument JsonOutput = QJsonDocument::fromJson(JsonData);
    QJsonObject JsonInfo = JsonOutput.object();
    if (JsonInfo.empty() == true)
    {
        JsonRetrievalError(bloctype);
    }
    else
    {
        try
        {
            QJsonObject block_metaObject = JsonInfo["block_meta"].toObject();
            QJsonObject block_idObject = block_metaObject["block_id"].toObject();
            QString BlockHash = block_idObject.value("hash").toString();

            QJsonObject HeaderObject = block_metaObject["header"].toObject();
            QString NumTxs = HeaderObject.value("num_txs").toString();
            QString Time = HeaderObject.value("time").toString();
            HeaderObject = HeaderObject["last_block_id"].toObject();
            QString LastBlockHash = HeaderObject.value("hash").toString();

            if(bloctype == BeforeLast)
            {
                ui->Hash_before->setText(BlockHash);
                ui->LastHash_before->setText(LastBlockHash);
                ui->BlockTime_before->setText(Time);
                ui->nbTx_before->setText(NumTxs);
            }
            else if (bloctype == Last)
            {
                ui->Hash_current->setText(BlockHash);
                ui->LastHash_current->setText(LastBlockHash);
                ui->BlockTime_current->setText(Time);
                ui->nbTx_current->setText(NumTxs);
            }
            else if (bloctype == Selected)
            {
                ui->Hash_selected->setText(BlockHash);
                ui->LastHash_selected->setText(LastBlockHash);
                ui->TimeBlock_selected->setText(Time);
                ui->nbTx_selected->setText(NumTxs);
            }
        }
        catch (exception e)       //If one Json data is not found, all fields are field with the warning
        {
            JsonRetrievalError(bloctype);
        }
    }
}


void BlockChainViewer::JsonRetrievalError(const BlockType blocktype)
{
    if(blocktype == BeforeLast)
    {
        ui->Hash_before->setText(JsonDataNotFound);
        ui->LastHash_before->setText(JsonDataNotFound);
        ui->BlockTime_before->setText(JsonDataNotFound);
        ui->nbTx_before->setText(JsonDataNotFound);
    }
    else if (blocktype == Last)
    {
        ui->Hash_current->setText(JsonDataNotFound);
        ui->LastHash_current->setText(JsonDataNotFound);
        ui->BlockTime_current->setText(JsonDataNotFound);
        ui->nbTx_current->setText(JsonDataNotFound);
    }
    else if (blocktype == Selected)
    {
        ui->Hash_selected->setText(JsonDataNotFound);
        ui->LastHash_selected->setText(JsonDataNotFound);
        ui->TimeBlock_selected->setText(JsonDataNotFound);
        ui->nbTx_selected->setText(JsonDataNotFound);
    }
}


void BlockChainViewer::HashCompare()
{
    if (ui->Hash_before->toPlainText() != JsonDataNotFound && !ui->Hash_before->toPlainText().isEmpty()
            && ui->Hash_current->toPlainText() != JsonDataNotFound && !ui->Hash_current->toPlainText().isEmpty())
    {
        if (ui->Hash_before->toPlainText() == ui->LastHash_current->toPlainText())
        {
            ui->Hash_before->setStyleSheet(BackgroundLightGreen);
            ui->LastHash_current->setStyleSheet(BackgroundLightGreen);
        }
        else
        {
            ui->Hash_before->setStyleSheet(BackgroundLightRed);
            ui->LastHash_current->setStyleSheet(BackgroundLightRed);
        }
    }
    else
    {
        ui->Hash_before->setStyleSheet(BackgroundWhite);
        ui->LastHash_current->setStyleSheet(BackgroundWhite);
    }
}


void BlockChainViewer::on_StartStopButton_clicked()
{
    if (ui->StartStopButton->icon().name() == "media-playback-stop")
    {
        timer->stop();
        ui->StartStopButton->setIcon(QIcon::fromTheme("media-playback-start"));
        ui->StartStopButton->setStyleSheet(BackgroundLightGreen);
        ui->StopStartLabel->setText("Start block data display");
    }
    else if (ui->StartStopButton->icon().name() == "media-playback-start")
    {
        timer->start(SavedInfo.TimerPeriod_ms);
        ui->StartStopButton->setIcon(QIcon::fromTheme("media-playback-stop"));
        ui->StartStopButton->setStyleSheet(BackgroundLightRed);
        ui->StopStartLabel->setText("Stop block data display");
    }
}


void BlockChainViewer::on_SelectedRadioButton_toggled(bool checked)
{
    ui->TransactionDate->setEnabled(checked);
    ui->TransactionTime->setEnabled(checked);
}


void BlockChainViewer::on_nbTx_before_textChanged(const QString &arg1)
{
    if (arg1.toInt() > 0)  //Check if at least one transaction is found
    {
        ui->nbTx_before->setStyleSheet(BackgroundLightGreen);
    }
}


void BlockChainViewer::on_nbTx_current_textChanged(const QString &arg1)
{
    if (arg1.toInt() > 0)  //Check if at least one transaction is found
    {
        ui->nbTx_current->setStyleSheet(BackgroundLightGreen);
    }
}


void BlockChainViewer::on_nbTx_selected_textChanged(const QString &arg1)
{
    if (arg1.toInt() > 0)  //Check if at least one transaction is found
    {
        ui->nbTx_selected->setStyleSheet(BackgroundLightGreen);
    }
}

void BlockChainViewer::on_filteredInfo1_textChanged()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_filteredInfo2_textChanged()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_filteredInfo3_textChanged()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_filteredInfo4_textChanged()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_filteredInfo5_textChanged()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_filteredInfo6_textChanged()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_CaseSensitiveCheckBox_toggled()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_TimerPeriod_valueChanged(int arg1)
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_BlockInfoBlinkingButton_toggled()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_EmptyTransactionsCheckBox_toggled()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_HistoricPath_textChanged()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_PortNumber_valueChanged(int arg1)
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_GaetwayModeCheckBox_toggled()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::on_SaveConfigButton_clicked()
{
    SavedInfo.unwantedInfo.clear();
    if(ui->filteredInfo1->text() != "")
    {SavedInfo.unwantedInfo.push_back(ui->filteredInfo1->text());}
    if(ui->filteredInfo2->text() != "")
    {SavedInfo.unwantedInfo.push_back(ui->filteredInfo2->text());}
    if(ui->filteredInfo3->text() != "")
    {SavedInfo.unwantedInfo.push_back(ui->filteredInfo3->text());}
    if(ui->filteredInfo4->text() != "")
    {SavedInfo.unwantedInfo.push_back(ui->filteredInfo4->text());}
    if(ui->filteredInfo5->text() != "")
    {SavedInfo.unwantedInfo.push_back(ui->filteredInfo5->text());}
    if(ui->filteredInfo6->text() != "")
    {SavedInfo.unwantedInfo.push_back(ui->filteredInfo6->text());}
    //No need to stop/start the timer if periodicity has not changed
    if (SavedInfo.TimerPeriod_ms != 1000*ui->TimerPeriod->value())
    {
        //transform from s to ms
        SavedInfo.TimerPeriod_ms = 1000*ui->TimerPeriod->value();
        timer->stop();
        timer->start(SavedInfo.TimerPeriod_ms);
    }
    SavedInfo.BlockInfoBlinking = ui->BlockInfoBlinkingButton->isChecked();
    /*---------------advanced settings--------------------*/
    SavedInfo.PortNumber = ui->PortNumber->value();
    SavedInfo.BlockchainStopQuit = ui->StopBlockchainCheckBox->isChecked();
    SavedInfo.EmptyTransactionsQuit = ui->EmptyTransactionsCheckBox->isChecked();
    SavedInfo.HistoricPath = ui->HistoricPath->text();
    SavedInfo.GatewayMode = ui->GaetwayModeCheckBox->isChecked();
    //re-deactivating the button
    ui->SaveConfigButton->setEnabled(false);
    //
    ui->StartStopGatewayButton->setEnabled(SavedInfo.GatewayMode);
    ui->StopStartGatewayLabel->setEnabled(SavedInfo.GatewayMode);
}


void BlockChainViewer::on_QuitButtonTab2_clicked()
{
    QuitProgram();
}

void BlockChainViewer::on_QuitButtonTab1_clicked()
{
    QuitProgram();
}

void BlockChainViewer::on_QuitButtonTab3_clicked()
{
    QuitProgram();
}

void BlockChainViewer::on_QuitButtonTab4_clicked()
{
    QuitProgram();
}



void BlockChainViewer::on_StopBlockchainCheckBox_toggled()
{
    ui->SaveConfigButton->setEnabled(true);
}

void BlockChainViewer::DeleteTransactionsHistoric()
{
    QFile *HistoricFile = new QFile(SavedInfo.HistoricPath);
    bool b = HistoricFile->open(QIODevice::ReadWrite | QIODevice::Text);
    if(b == true)
    {
        HistoricFile->resize(0);
        HistoricFile->close();
    }
    else
    {
        NotSupportedWarning("Not able to delete transactions historic");
    }
}


void BlockChainViewer::on_Creation_Button_clicked()
{
    CreationFunction();
}

void BlockChainViewer::on_DefaultConfigButton_clicked()
{
    //check if timer needs to be restarted
    bool restart = (SavedInfo.TimerPeriod_ms != DefaultConfig.TimerPeriod_ms) ? true : false;
    //revert to default display
    SetDefaultConfigDisplay();
    //retrieve default values
    SavedInfo = DefaultConfig;
    //Be careful, if default settings are chosen, gateway mode will be stopped
    GatewayTimer->stop();
    ui->StartStopGatewayButton->setIcon(QIcon::fromTheme("media-playback-start"));
    ui->StartStopGatewayButton->setStyleSheet(BackgroundLightGreen);
    ui->StopStartGatewayLabel->setText("Start gateway mode");

    if (restart == true)
    {
        timer->stop();
        timer->start();
    }
}

void BlockChainViewer::SetDefaultConfigDisplay()
{
    /*------------Transactions info filtering-----------------------*/
    ui->filteredInfo1->clear();
    ui->filteredInfo2->clear();
    ui->filteredInfo3->clear();
    ui->filteredInfo4->clear();
    ui->filteredInfo5->clear();
    ui->filteredInfo6->clear();
    ui->CaseSensitiveCheckBox->setChecked(DefaultCaseSensitive);
    /*--------------Display settings--------------------------*/
    ui->TimerPeriod->setValue(DefaultTimerSec);
    ui->BlockInfoBlinkingButton->setChecked(DefaultBlinking);
    /*---------------advanced settings----------------------------*/
    ui->PortNumber->setValue(DefaultPortNumber);
    ui->StopBlockchainCheckBox->setChecked(DefaultBlockchainStop);
    ui->EmptyTransactionsCheckBox->setChecked(DefaultTransactionsFlush);
    ui->HistoricPath->setText(DefaultHistoricPath);
    ui->GaetwayModeCheckBox->setChecked(DefaultGatewayMode);
    /******************************************/
    ui->SaveConfigButton->setEnabled(false);
    /*****************tab 2***********************/
    ui->StartStopGatewayButton->setEnabled(false);
    ui->StopStartGatewayLabel->setEnabled(false);
}


void BlockChainViewer::on_StartStopGatewayButton_clicked()
{
    if (ui->StartStopGatewayButton->icon().name() == "media-playback-stop")
    {
        GatewayTimer->stop();
        ui->StartStopGatewayButton->setIcon(QIcon::fromTheme("media-playback-start"));
        ui->StartStopGatewayButton->setStyleSheet(BackgroundLightGreen);
        ui->StopStartGatewayLabel->setText("Start gateway mode");
    }
    else if (ui->StartStopGatewayButton->icon().name() == "media-playback-start")
    {
        GatewayTimer->start(2000);
        ui->StartStopGatewayButton->setIcon(QIcon::fromTheme("media-playback-stop"));
        ui->StartStopGatewayButton->setStyleSheet(BackgroundLightRed);
        ui->StopStartGatewayLabel->setText("Stop gateway mode");
    }
}
