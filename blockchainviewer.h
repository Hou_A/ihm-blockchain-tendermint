#ifndef BLOCKCHAINVIEWER_H
#define BLOCKCHAINVIEWER_H

#include <QMainWindow>
#include <QObject>
#include <QProcess>
#include <iostream>
#include <QPushButton>
#include <QApplication>
#include <QtWidgets>
#include <QDebug>
#include <QLineEdit>
#include <QFormLayout>
#include <QFile>
#include <QThread>
#include <QJsonArray>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "commonfunctions.h"



namespace Ui {
class BlockChainViewer;
}


class BlockChainViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit BlockChainViewer(QWidget *parent = 0);
    ~BlockChainViewer();



private slots:

    void CreationFunction();

    void on_ResetButton_clicked();

    void DisplayInfo();

    void QuitProgram();

    void on_GetInfoButton_clicked();

    int TimestampsRequest();

    int GetClosestTimestamp(const QDateTime&);

    int FillInfo(QJsonObject& JsonInfo);

    void GetBlockChainInfo();

    void getFinished(QNetworkReply*);

    void GetURLInfo();

    void on_GetBlockInfoButton_clicked();

    QByteArray GetGeneralBlockInfo(int);

    void FillBlockGeneralInfo(const QByteArray, const BlockType = Random);

    void HashCompare();

    void GetBlockChainInfo_Part2();

    void on_StartStopButton_clicked();

    void on_SelectedRadioButton_toggled(bool checked);

    void ClearSelectedBlockDisplay();

    void on_nbTx_before_textChanged(const QString &arg1);

    void on_nbTx_current_textChanged(const QString &arg1);

    void on_nbTx_selected_textChanged(const QString &arg1);

    void ClearPeriodicInfo();

    void NoTransactionFoundDisplay(const QString&, const QString&);

    void JsonRetrievalError(const BlockType = Random);

    void on_filteredInfo1_textChanged();

    void on_filteredInfo2_textChanged();

    void on_filteredInfo3_textChanged();

    void on_filteredInfo4_textChanged();

    void on_filteredInfo5_textChanged();

    void on_filteredInfo6_textChanged();

    void on_CaseSensitiveCheckBox_toggled();

    void on_TimerPeriod_valueChanged(int arg1);             //arg1 is not used, just to avoid error message in Qt app output

    void on_BlockInfoBlinkingButton_toggled();

    void on_SaveConfigButton_clicked();

    void on_EmptyTransactionsCheckBox_toggled();

    void on_QuitButtonTab2_clicked();

    void on_QuitButtonTab1_clicked();

    void DeleteTransactionsHistoric();

    void on_QuitButtonTab3_clicked();

    void on_QuitButtonTab4_clicked();

    void on_HistoricPath_textChanged();

    void ModifyTextEdit(QTextEdit*, const QString, const bool = true );

    void ModifyLabel(QLabel*, const QString, const bool = true );

    void ResetTransactionDisplay(const int);

    void on_Creation_Button_clicked();

    void SetDefaultConfigDisplay();

    void on_BlockNumber_editingFinished();

    void GetSingleBlockInfo();

    void BroadcastTransaction();

    void on_DefaultConfigButton_clicked();

    void on_StopBlockchainCheckBox_toggled();

    void on_PortNumber_valueChanged(int arg1);                  //arg1 is not used, just to avoid error message in Qt app output

    void on_GaetwayModeCheckBox_toggled();

    void LaunchGatewayMode();

    void on_StartStopGatewayButton_clicked();

private:
    Ui::BlockChainViewer *ui;
    QVector<QString> OriginalTimestamps;
    QVector<QDateTime> TransactionDates;

    QTimer* timer;
    QTimer* GatewayTimer;
    QNetworkAccessManager* NetworkManager;
    QNetworkReply* NetworkReply;
    //Config info is constructed with default values
    ConfigInfo SavedInfo;
    const ConfigInfo DefaultConfig;
};


#endif // BLOCKCHAINVIEWER_H

