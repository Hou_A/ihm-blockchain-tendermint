#ifndef TIMESTAMPSTHREAD_H
#define TIMESTAMPSTHREAD_H

#include "blockchainviewer.h"
#include "ui_blockchainviewer.h"
#include <QMainWindow>
#include <QObject>
#include <QProcess>
#include <iostream>
#include <QPushButton>
#include <QApplication>
#include <QtWidgets>
#include <QDebug>
#include <QLineEdit>
#include <QFormLayout>
#include <QFile>
#include <QHoverEvent>
#include <QEvent>


class TimeStampsThread : public QThread
{
public:
    TimeStampsThread();
    TimeStampsThread(Ui::BlockChainViewer*);
    QString Output;
protected:
    void run(/*Ui::BlockChainViewer**/);

};


#endif // TIMESTAMPSTHREAD_H
