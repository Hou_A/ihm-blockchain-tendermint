#ifndef COMMONFUNCTIONS_H
#define COMMONFUNCTIONS_H

#include <QMessageBox>
#include <QDialog>
#include <QLineEdit>
#include <QProcess>
#include <QDateTime>
#include "globalparameters.h"
#include "generictypes.h"


void NotSupportedWarning(const QString);
QString EnterPassword();
void CloseAllTerminals();
int GoToDir(const QString&);
QDateTime TimestampToDate(const QString&);
QString FormatTimeStamp(const QString& );
void ChronoDates(QVector<QDateTime>);
void ChronoDates(QVector<QString>);     //function overloading
void SetDelay(int);
void KillSdProcess(int);


#endif // COMMONFUNCTIONS_H

