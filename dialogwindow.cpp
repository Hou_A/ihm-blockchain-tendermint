#include "dialogwindow.h"

DialogWindow::DialogWindow(QWidget *parent) : QWidget(parent)
{
    setFixedSize(300, 200);

    m_DialogButton = new QPushButton("Open Dialog Box", this);
    m_DialogButton->move(40, 50);

    m_PoliceButton = new QPushButton("Set Police", this);
    m_PoliceButton->move(100, 100);

    QObject::connect(m_DialogButton, SIGNAL(clicked()), this, SLOT(OpenDialog()));
    QObject::connect(m_PoliceButton, SIGNAL(clicked()), this, SLOT(SetButtonFont()));


}

void DialogWindow::OpenDialog()
{
    bool ok;
    QString pseudo = QInputDialog::getText(this, "Pseudo", "Quel est votre pseudo ?", QLineEdit::Normal, QString(), &ok);
    //QFont police = QFontDialog::getFont(&ok, m_boutonDialogue->font(), this, "Choisissez une police");
    if (ok /*&& !pseudo.isEmpty()*/)
    {
        if (pseudo.isEmpty())
        {
            QMessageBox::information(this, "Block Chain Viewer", "Please enter a pseudo");
        }
        else
        {
            int answer = QMessageBox::information(this, "Block Chain Viewer", "Welcome", QMessageBox::Yes | QMessageBox::No);
            if (answer == QMessageBox::Yes)
            {
                QMessageBox::information(this, "Block Chain Viewer", "Answer Yes");
                QString fichier = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Images (*.png *.gif *.jpg *.jpeg)");
                QMessageBox::information(this, "Fichier", "Vous avez sélectionné :\n" + fichier);
            }
            else if (answer == QMessageBox::No)
            {
                QMessageBox::information(this, "Block Chain Viewer", "Answer No");
                QString fichier = QFileDialog::getSaveFileName(this, "Enregistrer un fichier", QString(), "Images (*.png *.gif *.jpg *.jpeg)");
            }
        }
    }
}

void DialogWindow::SetButtonFont()
{
    bool ok;
    QFont police = QFontDialog::getFont(&ok, m_DialogButton->font(), this, "Choose a police");
    if (ok)
    {
        m_DialogButton->setFont(police);
    }
}



