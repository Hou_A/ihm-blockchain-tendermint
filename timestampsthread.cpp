#include "timestampsthread.h"

TimeStampsThread::TimeStampsThread()
{
    //TimeStampsThread::run(Ui::BlockChainViewer* ui);
}

TimeStampsThread::TimeStampsThread(Ui::BlockChainViewer* ui)
{
    //TimeStampsThread::run(Ui::BlockChainViewer* ui);
}


void TimeStampsThread::run(/*Ui::BlockChainViewer* ui*/)
{
    QProcess process;
    QTime dieTime= QTime::currentTime().addSecs(2);
    while (QTime::currentTime() < dieTime)
    {}
    process.start("scli query smartcity times");
    process.waitForFinished();
    QString output1 = process.readAll();
    this->Output = output1;
    //this->DisplayTimeStamps();
    process.close();
}
