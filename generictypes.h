#ifndef GENERICTYPES_H
#define GENERICTYPES_H


enum BlockType {Selected, BeforeLast, Last, Random};

struct ConfigInfo
{
    int TimerPeriod_ms;
    int PortNumber;
    bool BlockInfoBlinking;
    bool EmptyTransactionsQuit;
    bool CaseSensitive;
    bool BlockchainStopQuit;
    bool GatewayMode;
    QString HistoricPath;
    QStringList unwantedInfo;
    //default constructor
    ConfigInfo() : TimerPeriod_ms(DefaultTimerSec*1000), PortNumber(DefaultPortNumber), BlockInfoBlinking(DefaultBlinking), EmptyTransactionsQuit(DefaultTransactionsFlush)
      , CaseSensitive(DefaultCaseSensitive), BlockchainStopQuit(DefaultBlockchainStop), GatewayMode(DefaultGatewayMode), HistoricPath(DefaultHistoricPath)
    {
        //theoretically not needed as QStringList is per default empty
        unwantedInfo.clear();
    };
};

#endif // GENERICTYPES_H
