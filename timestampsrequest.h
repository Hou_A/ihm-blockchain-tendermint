#ifndef TIMESTAMPSREQUEST_H
#define TIMESTAMPSREQUEST_H

#include <QMainWindow>
#include <QObject>
#include <QProcess>


namespace Ui {
class TimestampsRequest;
}

class TimestampsRequest
{
    Q_OBJECT
public:
    TimestampsRequest();
     ~TimestampsRequest();

public slots:
    void process();

//signals:
//    void finished();
//    void error();

};

#endif // TIMESTAMPSREQUEST_H
