#include "mywindow.h"

MyWindow::MyWindow(QWidget *parent) : QWidget(parent)
{
    setFixedSize(600, 300);

    // Construction du bouton
    m_button = new QPushButton("Hello", this);
    CustomButtom (m_button, "Quit Button", "This program was developped with Qt");
    //
    m_lcd = new QLCDNumber(this);
    m_lcd->setSegmentStyle(QLCDNumber::Flat);
    m_lcd->move(50, 20);

    m_slider = new QSlider(Qt::Horizontal, this);
    m_slider->setGeometry(10, 60, 150, 20);
    m_slider->setRange(50, 100);



    m_progressbar = new QProgressBar(this);
    m_progressbar->setGeometry(10, 150, 150, 20);
    //
    //connexion between slider and lcd
    QObject::connect(m_slider, SIGNAL(valueChanged(int)), m_lcd, SLOT(display(int)));

    QObject::connect(m_slider, SIGNAL(valueChanged(int)), m_progressbar, SLOT(setValue(int)));

    QObject::connect(m_slider, SIGNAL(valueChanged(int)), this, SLOT(WidthChange(int)));

    QObject::connect(this, SIGNAL(MaxSize()), qApp, SLOT(quit()));


    //

    QObject::connect(m_button, SIGNAL(clicked()), qApp, SLOT(quit()));

//    MyWindow::~MyWindow()
//    {
//        delete (this->m_button);
//    }

}

void MyWindow::CustomButtom (QPushButton* button, const QString& Text, const QString& ToolTip)
{
    button->setText(Text);
    button->setToolTip(ToolTip);
    button->adjustSize();
    button->setFont(QFont("Comic Sans MS", 12));
    button->setCursor(Qt::PointingHandCursor);
    button->move(200, 100);
}

void MyWindow::WidthChange(int Width)
{
    setFixedSize(10*Width, 300);
    if (Width == 100)
    {
        emit MaxSize();
    }
}
