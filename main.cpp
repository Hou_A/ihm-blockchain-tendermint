#include "blockchainviewer.h"
#include <iostream>
#include <QDialog>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    BlockChainViewer w;

    w.setMinimumWidth(w.width());
    //w.showMaximized();
    w.show();

    return a.exec();
}
